[https://atlantica-conf.gitlab.io/guia-para-sobrevivir-fora-da-fic](https://atlantica-conf.gitlab.io/guia-para-sobrevivir-fora-da-fic)

# License
*Creative Commons BY-SA*.

# Autors
- *Pablo Castro Valiño*
- *Brais Arias Río*


# Description (Galician)

Como colaborar co software libre che axuda na vida profesional

Ano tras ano vemos como a tecnoloxía é un campo con moita saída laboral, moito sitio onde innovar e aprender cousas modernísimas e con tobogáns nas oficinas pero cando chegas a currar atópaste con código en Java 4, horas extra sen pagar e con unha cultura empresarial moi lonxe do que debería ser.

Nesta charla queremos falar da nosa experiencia polas diversas empresas polas que pasamos e facer unha intro ao noso sector para darche uns cuantos consellos prácticos co obxectivo de que a túa saída laboral sexa como ten que ser e non caias onde non debes ;)

Tamén veremos de que vai iso do software libre, de como a idea de compartir coñecemento está a cambiar o mundo e do importante que é para o teu futuro profesional xa que ao final, para as empresas que importan, participar no software libre é o teu novo currículum.
